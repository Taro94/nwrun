﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;

// ReSharper disable ClassNeverInstantiated.Global
// ReSharper disable ArrangeTypeModifiers
// ReSharper disable ArrangeTypeMemberModifiers

namespace NWRun
{
    class Program
    {
        private const string Version = "1.3.81935";
        private const string KillSignalString = "$awd2;22033pad92111330p#028!34@1";

        static void Main(string[] args)
        {
            if (args.Length < 1)
            {
                Console.WriteLine($"NWRun v{Version}");
                Console.WriteLine("Usage:");
                Console.WriteLine("nwrun <scriptfile> [-o <overrideDirectory>] [-t <secondsToLiveAfterExecution>] [-k <killSignalString>] [-l]");
                return;
            }
            
            // ReSharper disable once PossibleNullReferenceException
            var location = System.Reflection.Assembly.GetExecutingAssembly().CodeBase;
            var directory = Path.GetDirectoryName(location).Remove(0, 5);
            var overrideDir = Path.Combine(directory, "nwrserver/bin/override");
            var logDir = Path.Combine(directory, "nwrserver/bin/logs.0");
            var serverDir = Path.Combine(directory, "nwrserver/bin/linux-x86");
            
            var argAnalyzer = new ArgumentAnalyzer();
            var errorManager = new ErrorManager();
            var dirManager = new DirectoryManager();
            var preparation = new Preparation(dirManager, overrideDir);

            dirManager.EnsureDirectoryExists(overrideDir);
            dirManager.EnsureDirectoryExists(logDir);
            dirManager.ClearDirectory(overrideDir);
            dirManager.ClearDirectory(logDir);

            //var nwsArgs = new List<(string, string)>();
            //nwsArgs.Add(("myArg", "hello world from arg"));
            //bool createdInit = preparation.CreateInitScript(nwsArgs);
            //errorManager.ExitOnError(createdInit, "Failed to create the init script.");
            
            bool readScript = preparation.ReadScript(args[0], out string[] script);
            errorManager.ExitOnError(readScript, "Failed to read the script file.");

            bool createdMain = preparation.CreateMainScript(script);
            errorManager.ExitOnError(createdMain, "Failed to copy the script.");

            string timeArg = argAnalyzer.GetFlagValue(args, "-t");
            bool validTime = true;
            int serverShutdownDelay = 0;
            if (timeArg != null)
                validTime = preparation.GetTimeToLiveAfterExecution(timeArg, out serverShutdownDelay);
            errorManager.ExitOnError(validTime, "Time provided is invalid.");

            string customKillSignal = argAnalyzer.GetFlagValue(args, "-k");

            var createdKill = preparation.CreateKillScript(serverShutdownDelay, KillSignalString);
            errorManager.ExitOnError(createdKill, "Failed to initialize the server kill script.");

            string extraDirArg = argAnalyzer.GetFlagValue(args, "-o");
            bool validExtraDir = true;
            if (extraDirArg != null)
                validExtraDir = preparation.CopyFilesFromDirectoryToOverride(extraDirArg);
            errorManager.ExitOnError(validExtraDir, "Failed to account for the provided directory.");

            bool hideProcOutput = !args.Contains("-l");

            $"chmod 755 {Path.Combine(serverDir, "nwnsc")}".Bash();
            $"chmod 755 {Path.Combine(serverDir, "nwserver-linux")}".Bash();
            
            var compilerProcess = new Process()
            {
                StartInfo = new ProcessStartInfo()
                {
                    FileName = Path.Combine(serverDir, "nwnsc"),
                    Arguments = $"-n {Path.Combine(directory, "nwrserver")} {Path.Combine(directory, "nwrserver/bin/override/*.nss")} -y -i {overrideDir}",
                    RedirectStandardOutput = hideProcOutput,
                    RedirectStandardError = hideProcOutput,
                    UseShellExecute = false,
                    CreateNoWindow = true
                }
            };
            compilerProcess.Start();
            compilerProcess.WaitForExit();

            var serverProcess = new Process()
            {
                StartInfo = new ProcessStartInfo()
                {
                    FileName = Path.Combine(serverDir, "nwserver-linux"),
                    Arguments = $"-userdirectory {Path.Combine(directory, "nwrserver/bin")} -module nwrun -quiet",
                    WorkingDirectory = serverDir,
                    RedirectStandardOutput = hideProcOutput,
                    RedirectStandardError = hideProcOutput,
                    UseShellExecute = false,
                    CreateNoWindow = true
                }
            };
            
            var watcher = new LogWatcher(Path.Combine(logDir, "nwserverLog1.txt"), serverProcess, KillSignalString, customKillSignal);
            serverProcess.Start();
            serverProcess.WaitForExit();
        }
    }
}