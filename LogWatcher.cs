using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;

namespace NWRun
{
    public class LogWatcher
    {
        private readonly Process _server;
        private readonly string _filePath;
        private readonly string _killSignalString;
        private readonly string _customKillSignalString;
        
        private int _linesKnownNum = 2; //first lines of the log are not important
        private void OnChange(object sender, FileSystemEventArgs e)
        {
            var newLines = ReadNewLines();
            foreach (var line in newLines)
            {
                _linesKnownNum++;
                if ((line == _killSignalString && _customKillSignalString == null) 
                    || (line == _customKillSignalString && _customKillSignalString != null))
                {
                    _server.Kill();
                    return;
                }

                if (line != string.Empty 
                    && line != _killSignalString
                    && !line.StartsWith("Our public address as seen by the masterserver: ")
                    && !(line.StartsWith("Connection to ") && line.Contains(" relayed immediately: ")))
                    Console.WriteLine(line);
            }
        }

        private IEnumerable<string> ReadNewLines()
        {
            var lines = File.ReadLines(_filePath).Skip(_linesKnownNum);
            return lines;
        }

        public LogWatcher(string filePath, Process server, string killSignalString, string customKillSignalString=null)
        {
            _server = server;
            _filePath = filePath;
            _killSignalString = killSignalString;
            _customKillSignalString = customKillSignalString;
            // ReSharper disable once UseObjectOrCollectionInitializer
            var watcherInstance = new FileSystemWatcher();
            watcherInstance.Path = Path.GetDirectoryName(_filePath);
            watcherInstance.Filter = Path.GetFileName(_filePath);
            watcherInstance.Changed += OnChange;
            watcherInstance.EnableRaisingEvents = true;
        }
    }
}