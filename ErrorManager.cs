using System;

namespace NWRun
{
    public class ErrorManager
    {
        public void ExitOnError(bool correct, string exitErrorMessage)
        {
            if (correct)
                return;
            Console.WriteLine(exitErrorMessage);
            Environment.Exit(1);
        }
    }
}