using System;
using System.Collections.Generic;
using System.IO;

namespace NWRun
{
    public class Preparation
    {
        private readonly string _overrideDir;
        private DirectoryManager _dirManager;

        public Preparation(DirectoryManager dirManager, string overrideDir)
        {
            _overrideDir = overrideDir;
            _dirManager = dirManager;
        }
        
        public bool CreateInitScript(List<(string,string)> arguments)
        {
            try
            {
                using var writer = File.CreateText(Path.Combine(_overrideDir, "nwrun_init.nss"));
                writer.Write($"void main() {{ object mod = GetModule(); ");
                foreach (var (varName, varValue) in arguments)
                    writer.Write($"SetLocalString(mod, \"{varName}\", \"{varValue}\");");
                writer.Write("}");
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        
        public bool CreateKillScript(int serverShutdownDelay, string killSignalString)
        {
            try
            {
                using var writer = File.CreateText(Path.Combine(_overrideDir, "nwrun_kill.nss"));
                writer.Write($"void main() {{ DelayCommand({serverShutdownDelay}.0f, PrintString(\"{killSignalString}\")); }}");
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public bool ReadScript(string filePath, out string[] script)
        {
            try
            {
                script = File.ReadAllLines(filePath);
                return true;
            }
            catch (Exception)
            {
                script = new string[0];
                return false;
            }
        }
        
        public bool CreateMainScript(IEnumerable<string> script)
        {
            try
            {
                using var writer = File.CreateText(Path.Combine(_overrideDir, "nwrun_main.nss"));
                foreach (var line in script)
                    writer.WriteLine(line);
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public bool GetTimeToLiveAfterExecution(string timeArgument, out int seconds)
        {
            seconds = 0;
            try
            {
                seconds = int.Parse(timeArgument);
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public bool CopyFilesFromDirectoryToOverride(string sourceDirectory)
        {
            try
            {
                _dirManager.Copy(sourceDirectory, _overrideDir);
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }
    }
}