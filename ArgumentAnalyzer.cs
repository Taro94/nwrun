namespace NWRun
{
    public class ArgumentAnalyzer
    {
        public string GetFlagValue(string[] args, string flag)
        {
            string result = null;
            for (int i = 0; i < args.Length; i++)
            {
                if (args[i] != flag)
                    continue;
                if (i + 1 < args.Length)
                    result = args[i + 1];
                break;
            }

            return result;
        }
    }
}